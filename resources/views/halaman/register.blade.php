@extends('layout.master')

@section('Judul')
Halaman Pendaftaran
@endsection

@section('content')
    
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</Form></h4>

    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name :</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" value="11" name="gender">Male<br>
        <input type="radio" value="22" name="gender">Female<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" value="33">Bahasa Indonesia<br>
        <input type="checkbox" value="44">English<br>
        <input type="checkbox" value="55">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>



@endsection