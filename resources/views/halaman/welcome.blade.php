@extends('layout.master')

@section('Judul')
Selamat Datang
@endsection

@section('content')

    <h1>SELAMAT DATANG! {{$fname}} {{$lname}}</h1>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita Bersama!</h3>
@endsection