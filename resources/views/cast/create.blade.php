@extends('layout.master')

@section('Judul')
Halaman Create Cast 
@endsection

@section('content')
<form method="POST" action="/cast">
    @csrf
<div class="form-grup">
    <label>Nama Cast</label>
    <input type="text" name="nama" class="form-control">
</div>
@error('nama')
    <div class="alert alert-danger">{{$message}}</div>
@enderror
<div class="form-grup">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control">
</div>
@error('umur')
    <div class="alert alert-danger">{{$message}}</div>
@enderror
<div class="form-grup">
    <label>Bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
</div>
@error('bio')
    <div class="alert alert-danger">{{$message}}</div>
@enderror
<br>
<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection