@extends('layout.master')

@section('Judul')
Halaman Edit Cast 
@endsection

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
<div class="form-grup">
    <label>Nama Cast</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
</div>
@error('nama')
    <div class="alert alert-danger">{{$message}}</div>
@enderror
<div class="form-grup">
    <label>Umur</label>
    <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
</div>
@error('umur')
    <div class="alert alert-danger">{{$message}}</div>
@enderror
<div class="form-grup">
    <label>Bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
</div>
@error('bio')
    <div class="alert alert-danger">{{$message}}</div>
@enderror
<br>
<button type="submit" class="btn btn-primary">Update Data</button>
</form>
@endsection