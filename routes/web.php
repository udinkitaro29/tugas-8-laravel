<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', 'IndexController@table');


//CRUD Cast
//create
Route::get('/cast/create', 'CastController@create'); //form create
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database

//read
Route::get('/cast', 'CastController@index'); //ambil data
Route::get('/cast/{cast_id}', 'CastController@show'); //detail cast

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route edit
Route::put('/cast/{cast_id}', 'CastController@update'); //Route update

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route hapus data berdasar id


